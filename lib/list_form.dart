import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ListForm extends StatefulWidget {
  String listId;
  ListForm({Key? key, required this.listId}) : super(key: key);

  @override
  _ListFormState createState() => _ListFormState(this.listId);
}

class _ListFormState extends State<ListForm> {
  String listId;
  String listName = "";
  String detail = "";
  CollectionReference lists = FirebaseFirestore.instance.collection('lists');
  _ListFormState(this.listId);
  TextEditingController _listNameController = new TextEditingController();
  TextEditingController _detailController = new TextEditingController();
  @override
  void initState() {
    super.initState();
    if (this.listId.isNotEmpty) {
      lists.doc(this.listId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          listName = data['name'];
          detail = data['detail'];
          _listNameController.text = listName;
          _detailController.text = detail;
        }
      });
    }
  }

  final _formKey = GlobalKey<FormState>();
  Future<void> addList() {
    return lists
        .add({
          'list_name': this.listName,
          'detail': this.detail,
        })
        .then((value) => print('list Added' + '$listName' + '$detail'))
        .catchError((error) => print('Failed to add list: $error'));
  }

  Future<void> updateList() {
    return lists
        .doc(this.listId)
        .update({
          'detail': detail,
          'list_name': this.listName,
        })
        .then((value) => print("list Updated"))
        .catchError((error) => print("Failed to update list: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('จัดการรายการ'), backgroundColor: Color(0xff885566)),
      body: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: [
              TextFormField(
                controller: _listNameController,
                decoration: InputDecoration(labelText: 'ชื่อรายการ'),
                onChanged: (value) {
                  setState(() {
                    listName = value;
                  });
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'โปรดใส่ชื่อรายการ';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 15.0,
              ),
              TextFormField(
                maxLines: 2,
                controller: _detailController,
                decoration: InputDecoration(labelText: 'รายละเอียดรายการ'),
                onChanged: (value) {
                  setState(() {
                    detail = value;
                  });
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'โปรดใส่รายละเอียดรายการ';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 15.0,
              ),
              ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (listId.isEmpty) {
                        await addList();
                      } else {
                        await updateList();
                      }
                      Navigator.pop(context);
                    }
                  },
                  style: ElevatedButton.styleFrom(
                      primary: Colors.green.shade400,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 15),
                      textStyle: const TextStyle(
                          fontSize: 20, fontWeight: FontWeight.bold)),
                  child: Text('บันทึก'))
            ],
          ),
        ),
      ),
    );
  }
}
