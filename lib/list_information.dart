import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:example_project/list_form.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

class ListInformation extends StatefulWidget {
  ListInformation({Key? key}) : super(key: key);

  @override
  _ListInformationState createState() => _ListInformationState();
}

class _ListInformationState extends State<ListInformation> {
  final Stream<QuerySnapshot> _listStream = FirebaseFirestore.instance
      .collection('lists')
      .orderBy('list_name', descending: false)
      .snapshots();
  CollectionReference lists = FirebaseFirestore.instance.collection('lists');
  Future<void> delList(listId) {
    return lists
        .doc(listId)
        .delete()
        .then((value) => print('list Delete'))
        .catchError((error) => print('Failed to delete list: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _listStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text("Something went wrong");
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }
          return ListView(children: [
            Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: snapshot.data!.docs.map((DocumentSnapshot document) {
                  Map<String, dynamic> data =
                      document.data()! as Map<String, dynamic>;
                  return ListTile(
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
                    title: Text(
                      data['list_name'],
                      style: TextStyle(fontSize: 25.0),
                    ),
                    subtitle: Text(
                      data['detail'],
                      style: TextStyle(fontSize: 18.0),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  ListForm(listId: document.id)));
                    },
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        IconButton(
                          icon: Icon(Icons.share),
                          color: Colors.blue.shade400,
                          onPressed: () {
                            Share.share(
                                '${data['list_name']}\n${data['detail']}');
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.delete),
                          color: Colors.red.shade400,
                          onPressed: () async {
                            await delList(document.id);
                          },
                        ),
                      ],
                    ),
                  );
                }).toList(),
              ),
            ),
          ]);
        });
  }
}
