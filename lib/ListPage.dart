import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:example_project/list_form.dart';
import 'package:example_project/list_information.dart';
import 'package:flutter/material.dart';

class ListPage extends StatefulWidget {
  ListPage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  CollectionReference lists = FirebaseFirestore.instance.collection('lists');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xff885566),
        title: Text(widget.title),
      ),
      body: Center(child: ListInformation()),
      floatingActionButton: ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: Colors.green.shade400,
            padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
            textStyle:
                const TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
        child: const Text('เพิ่มรายการ'),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ListForm(listId: '')));
        },
      ),
    );
  }
}
