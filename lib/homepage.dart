import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:example_project/ListPage.dart';
import 'package:flutter/material.dart';

class MyHomePage1 extends StatefulWidget {
  MyHomePage1({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage1> {
  CollectionReference lists = FirebaseFirestore.instance.collection('lists');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue.shade100,
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Icon(
            Icons.shopping_cart,
            size: 100,
          ),
          SizedBox(height: 20),
          Text(
            'WELCOME TO BUYSTILL',
            textAlign: TextAlign.center,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
          ),
          SizedBox(height: 20),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.green.shade400,
                padding:
                    const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                textStyle:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            child: const Text('เข้าสู้หน้ารายการ'),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ListPage(
                            title: ('หน้ารายการ'),
                          )));
            },
          )
        ]),
      ),
    );
  }
}
